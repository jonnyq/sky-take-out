package com.sky.exception;

public class WrongDateException extends BaseException {

    public WrongDateException(String msg) {
        super(msg);
    }

}
