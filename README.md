# sky-take-out

#### 介绍
    
用于餐饮业的软件产品。包括 系统管理后台 和 小程序端应用 两部分。其中系统管理后台主要提供给餐饮企业内部员工使用，可以对餐厅的分类、菜品、套餐、订单、员工等进行管理维护，对餐厅的各类数据进行统计，同时也可进行来单语音，催单语音播报功能。
    小程序端主要提供给消费者使用，可以在线浏览菜品、添加购物车、下单、支付、催单等。
![输入图片说明](sky-server/src/main/resources/Snipaste_2024-10-13_13-33-48.png)
#### 技术选型

- 用户层：node.js VUE.js ElementUI 微信小程序 apache Echarts
- 网关层：nginx
- 应用层：Spring Boot, Spring MVC, Spring Task, HTTP Client, Spring Cache, JWT, 阿里云OSS, swagger, POI, WebSocket
- 数据层：MySQL, Redis, Mybatis, page Helper, Spring data Redis
- 工具：git maven junit Postman

#### 安装教程
1.  前端环境搭建（管理端网页），直接下载nginx相关文件包，解压运行即可。
2.  用户端模拟：将相关代码导入微信开发者工具，编译即可。
3.  启用Redis：在Redis根目录下，使用cmd命令行redis-server.exe redis.windows.conf（Redis服务默认端口号为 6379 ，通过快捷键Ctrl + C 即可停止Redis服务）
4.  启用Websocket：需启用cpolar进行内网穿透，使管理端网页与小程序握手连接。
5.  相关环境zip文件放在sky-server下的resources中，解压即可用

#### 使用说明

1.  部分代码与视频所教的不同。
2.  需要在application-dev.yml中进行相应的配置，具体如下：![输入图片说明](sky-server/src/main/resources/Snipaste_2024-09-14_17-02-52.png)
3.  使用了apifox对接口进行管理调试。（原Yapi）
4.  代码大体结构
![输入图片说明](sky-server/src/main/resources/Snipaste_2024-10-13_13-27-49.png)


