package com.sky.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.constant.MessageConstant;
import com.sky.constant.StatusConstant;
import com.sky.dto.SetmealDTO;
import com.sky.dto.SetmealPageQueryDTO;
import com.sky.entity.Setmeal;
import com.sky.entity.SetmealDish;
import com.sky.exception.DeletionNotAllowedException;
import com.sky.mapper.SetmealDishMapper;
import com.sky.mapper.SetmealMapper;
import com.sky.result.PageResult;
import com.sky.service.SetmealService;
import com.sky.vo.DishItemVO;
import com.sky.vo.SetmealVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SetmealServiceImpl implements SetmealService {
    @Autowired
    private SetmealMapper setmealMapper;
    @Autowired
    private SetmealDishMapper setmealDishMapper;

    /**
     * 新增套餐
     * @param setmealDTO
     */
    @Override
    public void saveSetmeal(SetmealDTO setmealDTO) {
        //套餐新增
        Setmeal setmeal=new Setmeal();
        BeanUtils.copyProperties(setmealDTO,setmeal);
        setmealMapper.insertSetmeal(setmeal);
        //绑定套餐id
        Long setmealId=setmeal.getId();
        List<SetmealDish> setmealDishList=setmealDTO.getSetmealDishes();
        setmealDishList.forEach(setmealDish -> {
            setmealDish.setSetmealId(setmealId);
        });
        //插入相关菜品至关联表
        if(setmealDishList!=null && setmealDishList.size()>0) {
            setmealDishMapper.insertSetmealDishes(setmealDishList);
        }
    }

    /**
     * 分页查询
     * @param setmealPageQueryDTO
     * @return
     */
    @Override
    public PageResult pageQuerySetmeal(SetmealPageQueryDTO setmealPageQueryDTO) {
        //分页
        PageHelper.startPage(setmealPageQueryDTO.getPage(),setmealPageQueryDTO.getPageSize());
        Page<SetmealVO> page=setmealMapper.getByPage(setmealPageQueryDTO);
        Long total=page.getTotal();
        List<SetmealVO> record=page.getResult();
        return new PageResult(total,record);
    }

    /**
     * 根据套餐id查询套餐
     * @param id
     * @return
     */
    @Override
    public SetmealVO getSetmealById(Long id) {
        //查询套餐信息
        Setmeal setmeal=setmealMapper.getById(id);
        //查询相关菜品信息
        List<SetmealDish> setmealDishList=setmealDishMapper.getSetmealDishById(id);
        //信息回显
        SetmealVO setmealVO=new SetmealVO();
        BeanUtils.copyProperties(setmeal,setmealVO);
        setmealVO.setSetmealDishes(setmealDishList);
        return setmealVO;
    }

    /**
     * 更新套餐信息
     * @param setmealDTO
     */
    @Override
    public void updateSetmealWithDish(SetmealDTO setmealDTO) {
        //更新套餐表
        Setmeal setmeal=new Setmeal();
        BeanUtils.copyProperties(setmealDTO,setmeal);
        setmealMapper.updateSetmeal(setmeal);
        //删除关联表中菜品
        setmealDishMapper.deleteSetmealDishById(setmealDTO.getId());
        //向关联表重新插入菜品信息
        Long setmealId=setmealDTO.getId();
        List<SetmealDish> setmealDishList=setmealDTO.getSetmealDishes();
        //关联表中菜品绑定套餐id
        setmealDishList.forEach(setmealDish -> {
            setmealDish.setSetmealId(setmealId);
        });
        if(setmealDishList!=null && setmealDishList.size()>0) {
            setmealDishMapper.insertSetmealDishes(setmealDishList);
        }
    }

    /**
     * 启售禁售套餐
     * @param id
     */
    @Override
    public void enableOrDisable(Integer status, Long id) {
        Setmeal setmeal = Setmeal.builder()
                .status(status)
                .id(id).build();
        setmealMapper.updateSetmeal(setmeal);
    }

    /**
     * 批量删除
     * @param ids
     */
    @Override
    public void deleteSetmealBatch(List<Long> ids) {
        for(Long id:ids){
            Setmeal setmeal=setmealMapper.getById(id);
            if(setmeal.getStatus()== StatusConstant.ENABLE){
                throw new DeletionNotAllowedException(MessageConstant.DISH_ON_SALE);
            }
        }
        //删除套餐
        setmealMapper.deleteSetmealByids(ids);
        //删除关联表中相关菜品
        setmealDishMapper.deleteSetmealDishByIds(ids);
    }
    /**
     * 条件查询
     * @param setmeal
     * @return
     */
    public List<Setmeal> list(Setmeal setmeal) {
        List<Setmeal> list = setmealMapper.getSetmealByIfs(setmeal);
        return list;
    }

    /**
     * 根据id查询菜品选项
     * @param id
     * @return
     */
    public List<DishItemVO> getDishItemById(Long id) {
        return setmealMapper.getDishItemBySetmealId(id);
    }
}
