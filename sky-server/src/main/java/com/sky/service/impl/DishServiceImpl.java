package com.sky.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.constant.MessageConstant;
import com.sky.constant.StatusConstant;
import com.sky.dto.DishDTO;
import com.sky.dto.DishPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.entity.DishFlavor;
import com.sky.exception.DeletionNotAllowedException;
import com.sky.mapper.DishFlavorMapper;
import com.sky.mapper.DishMapper;
import com.sky.mapper.SetmealDishMapper;
import com.sky.result.PageResult;
import com.sky.service.DishService;
import com.sky.vo.DishVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class DishServiceImpl implements DishService {
    @Autowired
    private DishMapper dishMapper;
    @Autowired
    private DishFlavorMapper dishFlavorMapper;
    @Autowired
    private SetmealDishMapper setmealDishMapper;
    /**
     * 新增菜品
     * @param dishDTO
     */
    @Transactional
    @Override
    public void saveDishWithFlavor(DishDTO dishDTO) {
        //向菜品表插入一条记录
        Dish dish=new Dish();
        BeanUtils.copyProperties(dishDTO,dish);
        dishMapper.insertDish(dish);
        //主键回显
        Long id =dish.getId();
        //向口味表插入多条记录
        List<DishFlavor> dishFlavors=dishDTO.getFlavors();
        dishFlavors.forEach(dishFlavor -> {
            dishFlavor.setDishId(id);
        });
        if(dishFlavors!=null && dishFlavors.size()>0){
            dishFlavorMapper.insertFlavors(dishFlavors);
        }

    }

    /**
     * 菜品分页查询
     * @param dishPageQueryDTO
     * @return
     */
    @Override
    public PageResult queryDishByPage(DishPageQueryDTO dishPageQueryDTO) {
        //开始分页
        PageHelper.startPage(dishPageQueryDTO.getPage(),dishPageQueryDTO.getPageSize());
        Page<DishVO> page=dishMapper.selectDishByPage(dishPageQueryDTO);
        long total = page.getTotal();
        List<DishVO> dishes= page.getResult();
        return new PageResult(total,dishes);
    }

    /**
     * 批量删除菜品
     * @param ids
     */
    @Override
    public void deleteBatch(List<Long> ids) {
        //判断菜品是否启售
        for (Long id:ids){
            Dish dish=dishMapper.getById(id);
            if(dish.getStatus()== StatusConstant.ENABLE){
                throw new  DeletionNotAllowedException(MessageConstant.DISH_ON_SALE);
            }
        }
        //判断菜品是否在套餐中
        List<Long> setMealIds =setmealDishMapper.getSetmealIdsByDishIds(ids);
        if(setMealIds!=null && setMealIds.size()>0){
            throw  new DeletionNotAllowedException(MessageConstant.DISH_BE_RELATED_BY_SETMEAL);
        }

           //删除菜品数据
           dishMapper.deleteDishByIds(ids);
           //删除菜品口味数据
           dishFlavorMapper.deleteDishFlavorByDishIds(ids);
    }

    /**
     * 根据id获取菜品信息
     * @param id
     * @return
     */
    @Override
    public DishVO getDishByIdWithFlavor(Long id) {
        //获取菜品信息
        Dish dish=dishMapper.getById(id);
        //获取口味信息
        List<DishFlavor> dishFlavors= dishFlavorMapper.getFlavorByDishId(id);
        //菜品口味信息前端回显
        DishVO dishVO=new DishVO();
        BeanUtils.copyProperties(dish,dishVO);
        dishVO.setFlavors(dishFlavors);
        return dishVO;
    }

    /**
     * 根据id修改菜品信息
     * @param dishDTO
     */
    @Override
    public void updateDish(DishDTO dishDTO) {
        //菜品信息更新
        Dish dish = new Dish();
        BeanUtils.copyProperties(dishDTO,dish);
        dishMapper.updateDish(dish);
        //删除对应菜品的口味信息
        List<Long> dishIds = new ArrayList<>();
        Long dishId=dishDTO.getId();
        dishIds.add(dishId);
        dishFlavorMapper.deleteDishFlavorByDishIds(dishIds);
        //向口味表重新插入多条记录
        List<DishFlavor> dishFlavors=dishDTO.getFlavors();
        dishFlavors.forEach(dishFlavor -> {
            dishFlavor.setDishId(dishId);
        });
        if(dishFlavors!=null && dishFlavors.size()>0){
            dishFlavorMapper.insertFlavors(dishFlavors);
        }
    }

    /**
     * 根据类别查询菜品
     * @param categoryId
     * @return
     */
    @Override
    public List<Dish> getDishByCategoryId(Long categoryId) {
        List<Dish> dishList=dishMapper.selectDishByCategoryId(categoryId);
        return dishList;
    }
    /**
     * 条件查询菜品和口味
     * @param dish
     * @return
     */
    @Override
    public List<DishVO> listWithFlavor(Dish dish) {
        List<Dish> dishList = dishMapper.selectDishByIfs(dish);

        List<DishVO> dishVOList = new ArrayList<>();

        for (Dish d : dishList) {
            DishVO dishVO = new DishVO();
            BeanUtils.copyProperties(d,dishVO);

            //根据菜品id查询对应的口味
            List<DishFlavor> flavors = dishFlavorMapper.getFlavorByDishId(d.getId());

            dishVO.setFlavors(flavors);
            dishVOList.add(dishVO);
        }

        return dishVOList;
    }

    /**
     * 启用禁用菜品
     * @param status
     */
    @Override
    public void enableOrDisable(Integer status,Long id) {
        Dish dish = new Dish();
        dish.setId(id);
        dish.setStatus(status);
        dishMapper.updateDish(dish);
    }
}
