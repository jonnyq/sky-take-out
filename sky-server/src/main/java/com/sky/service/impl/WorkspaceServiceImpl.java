package com.sky.service.impl;

import com.sky.constant.StatusConstant;
import com.sky.entity.Orders;
import com.sky.mapper.DishMapper;
import com.sky.mapper.OrderMapper;
import com.sky.mapper.SetmealMapper;
import com.sky.mapper.UserMapper;
import com.sky.service.WorkspaceService;
import com.sky.vo.BusinessDataVO;
import com.sky.vo.DishOverViewVO;
import com.sky.vo.OrderOverViewVO;
import com.sky.vo.SetmealOverViewVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class WorkspaceServiceImpl implements WorkspaceService {

    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private DishMapper dishMapper;
    @Autowired
    private SetmealMapper setmealMapper;

    /**
     * 根据时间段统计营业数据
     * @param begin
     * @param end
     * @return
     */
    public BusinessDataVO getBusinessData(LocalDate begin,LocalDate end) {
        /**
         * 营业额：当日已完成订单的总金额
         * 有效订单：当日已完成订单的数量
         * 订单完成率：有效订单数 / 总订单数
         * 平均客单价：营业额 / 有效订单数
         * 新增用户：当日新增用户的数量
         */

        Map map = new HashMap();
        map.put("begin",begin);
        map.put("end",end);

        //查询总订单数
        List<Integer> totalOrderCountList = orderMapper.countDayOrders(map);

        map.put("status", Orders.COMPLETED);
        //营业额
        List<BigDecimal> turnoverList = orderMapper.getTurnover(map);
        BigDecimal turnover = turnoverList.get(0) == null? BigDecimal.valueOf(0.00) : turnoverList.get(0);

        //有效订单数
        List<Integer> validOrderCountList = orderMapper.countDayOrders(map);

        Double unitPrice = 0.0;

        Double orderCompletionRate = 0.0;
        Integer totalOrderCount=totalOrderCountList.get(0);
        Integer validOrderCount=validOrderCountList.get(0);
        if(totalOrderCount != 0 && validOrderCount != 0){
            //订单完成率
            orderCompletionRate = validOrderCount.doubleValue() / totalOrderCount;
            //平均客单价
            unitPrice = turnover.doubleValue() / validOrderCount;
        }

        //新增用户数
        List<Integer> newUsersCountList = userMapper.countNewUser(begin,end);

        return BusinessDataVO.builder()
                .turnover(turnover.doubleValue())
                .validOrderCount(validOrderCount)
                .orderCompletionRate(orderCompletionRate)
                .unitPrice(unitPrice)
                .newUsers(newUsersCountList.get(0))
                .build();
    }


    /**
     * 查询订单管理数据
     *
     * @return
     */
    public OrderOverViewVO getOrderOverView() {
        Map map = new HashMap();
        map.put("begin", LocalDate.now());
        map.put("end",LocalDate.now());
        map.put("status", Orders.TO_BE_CONFIRMED);

        //待接单
        List<Integer> waitingOrders = orderMapper.countDayOrders(map);

        //待派送
        map.put("status", Orders.CONFIRMED);
        List<Integer> deliveredOrders = orderMapper.countDayOrders(map);

        //已完成
        map.put("status", Orders.COMPLETED);
        List<Integer> completedOrders = orderMapper.countDayOrders(map);

        //已取消
        map.put("status", Orders.CANCELLED);
        List<Integer> cancelledOrders = orderMapper.countDayOrders(map);

        //全部订单
        map.put("status", null);
        List<Integer> allOrders = orderMapper.countDayOrders(map);

        return OrderOverViewVO.builder()
                .waitingOrders(waitingOrders.get(0))
                .deliveredOrders(deliveredOrders.get(0))
                .completedOrders(completedOrders.get(0))
                .cancelledOrders(cancelledOrders.get(0))
                .allOrders(allOrders.get(0))
                .build();
    }

    /**
     * 查询菜品总览
     *
     * @return
     */
    public DishOverViewVO getDishOverView() {
        Map map = new HashMap();
        map.put("status", StatusConstant.ENABLE);
        Integer sold = dishMapper.countByMap(map);

        map.put("status", StatusConstant.DISABLE);
        Integer discontinued = dishMapper.countByMap(map);

        return DishOverViewVO.builder()
                .sold(sold)
                .discontinued(discontinued)
                .build();
    }

    /**
     * 查询套餐总览
     *
     * @return
     */
    public SetmealOverViewVO getSetmealOverView() {
        Map map = new HashMap();
        map.put("status", StatusConstant.ENABLE);
        Integer sold = setmealMapper.countByMap(map);

        map.put("status", StatusConstant.DISABLE);
        Integer discontinued = setmealMapper.countByMap(map);

        return SetmealOverViewVO.builder()
                .sold(sold)
                .discontinued(discontinued)
                .build();
    }


}
