package com.sky.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.sky.constant.MessageConstant;
import com.sky.dto.UserLoginDTO;
import com.sky.entity.User;
import com.sky.exception.LoginFailedException;
import com.sky.mapper.UserMapper;
import com.sky.properties.WeChatProperties;
import com.sky.service.UserService;
import com.sky.utils.HttpClientUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@Service
public class UserServiceImpl implements UserService {

    public final static String WX_LOGIN = "https://api.weixin.qq.com/sns/jscode2session";
    @Autowired
    private WeChatProperties weChatProperties;
    @Autowired
    private UserMapper userMapper;
    /**
     * 微信用户登录
     * @param userLoginDTO
     * @return
     */
    @Override
    public User wxLogin(UserLoginDTO userLoginDTO) {
        String openid = getOpenId(userLoginDTO);
        //openid为空，则登录失败，抛异常
        if(openid ==null){
            throw new LoginFailedException(MessageConstant.LOGIN_FAILED);
        }
        //判断是否为新用户,若是则自动注册
        User user = userMapper.getByOpenId(openid);
        if(user==null){
               user = User.builder()
                    .openid(openid)
                    .createTime(LocalDateTime.now())
                    .build();
               userMapper.insertUser(user);
        }

        return user;
    }

    /**
     * 获得微信用户openid
     * @param userLoginDTO
     * @return
     */
    private String getOpenId(UserLoginDTO userLoginDTO) {
        //调用微信接口服务，获取当前用户的openid
        Map<String, String> map=new HashMap<>();
        map.put("appid",weChatProperties.getAppid());
        map.put("secret",weChatProperties.getSecret());
        map.put("js_code",userLoginDTO.getCode());
        map.put("grant_type","authorization_code");
        //获得响应体
        String json = HttpClientUtil.doGet(WX_LOGIN, map);
        //JSON将JSON类型的数据格式转成Java对象，通过getter方法取出对象的属性
        JSONObject jsonObject= JSON.parseObject(json);
        return jsonObject.getString("openid");
    }
}
