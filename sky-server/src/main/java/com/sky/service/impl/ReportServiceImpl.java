package com.sky.service.impl;

import com.sky.dto.GoodsSalesDTO;
import com.sky.entity.Orders;
import com.sky.exception.WrongDateException;
import com.sky.mapper.OrderMapper;
import com.sky.mapper.UserMapper;
import com.sky.service.ReportService;
import com.sky.service.WorkspaceService;
import com.sky.vo.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ReportServiceImpl implements ReportService {

    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private WorkspaceService workspaceService;
    /**
     * 统计指定时间区间内的营业额数据
     * @param begin
     * @param end
     * @return
     */
    @Override
    public TurnoverReportVO getTurnoverStatistics(LocalDate begin, LocalDate end) {
        if(begin.isAfter(end)) {
            throw new WrongDateException("开始日期不能大于结束日期");
        }
        //获取范围内日期
        List<LocalDate> dateList = new ArrayList<>();
        LocalDate date=begin;
        dateList.add(begin);
        while(!date.equals(end)){
            date=date.plusDays(1);
            dateList.add(date);
        }

        //获取营业额
        Map map=new HashMap();
        map.put("begin",begin);
        map.put("end",end);
        map.put("status",Orders.COMPLETED);
        List<BigDecimal> turnoverList=orderMapper.getTurnover(map);

        TurnoverReportVO turnoverReportVO=new TurnoverReportVO();
        turnoverReportVO.setDateList(StringUtils.join(dateList,","));
        turnoverReportVO.setTurnoverList(StringUtils.join(turnoverList,","));
        return turnoverReportVO;
    }

    /**
     * 统计指定时间区间内的用户数据
     * @param begin
     * @param end
     * @return
     */
    @Override
    public UserReportVO getUserStatistics(LocalDate begin, LocalDate end) {

        if(begin.isAfter(end)) {
            throw new WrongDateException("开始日期不能大于结束日期");
        }
        //获取范围内日期
        List<LocalDate> dateList = new ArrayList<>();
        LocalDate date=begin;
        dateList.add(begin);
        while(!date.equals(end)){
            date=date.plusDays(1);
            dateList.add(date);
        }

        //每日新增用户统计
        List<Integer> newUserList = userMapper.countNewUser(begin,end);

        //用户总量统计
        List<Integer> totalUserList = new ArrayList<>();
        Integer beginTotal = userMapper.countTotalUser(begin);
        Integer total=beginTotal-newUserList.get(0);
        for (Integer newUserNumber : newUserList) {
            total=total+newUserNumber;
            totalUserList.add(total);
        }

        UserReportVO userReportVO = UserReportVO.builder()
                .dateList(StringUtils.join(dateList, ","))
                .newUserList(StringUtils.join(newUserList, ","))
                .totalUserList(StringUtils.join(totalUserList, ","))
                .build();

        return userReportVO;
    }

    /**
     * 订单数据统计
     * @param begin
     * @param end
     * @return
     */
    @Override
    public OrderReportVO getOrdersStatistics(LocalDate begin, LocalDate end) {
        if(begin.isAfter(end)) {
            throw new WrongDateException("开始日期不能大于结束日期");
        }
        //获取范围内日期
        List<LocalDate> dateList = new ArrayList<>();
        LocalDate date=begin;
        dateList.add(begin);
        while(!date.equals(end)){
            date=date.plusDays(1);
            dateList.add(date);
        }
        //获取每日订单数量列表
        Map map=new HashMap();
        map.put("begin",begin);
        map.put("end",end);
        List<Integer> orderCountList= orderMapper.countDayOrders(map);
        //获取每日有效订单数量列表
        map.put("status",Orders.COMPLETED);
        List<Integer> validOrderCountList = orderMapper.countDayOrders(map);
        //获取订单总数
        Integer totalOrder = 0;
        for (Integer orderCount : orderCountList) {
            totalOrder=totalOrder+orderCount;
        }
        //获取有效订单总数
        Integer totalValidOrder =0;
        for (Integer validOrderCount : validOrderCountList) {
            totalValidOrder=totalValidOrder+validOrderCount;
        }
        //计算订单完成率
        Double orderCompletionRate=0.0;
        if (totalOrder!=0){
           orderCompletionRate=totalValidOrder.doubleValue()/totalOrder;
        }
        OrderReportVO orderReportVO = OrderReportVO.builder()
                .dateList(StringUtils.join(dateList, ","))
                .orderCountList(StringUtils.join(orderCountList, ","))
                .totalOrderCount(totalOrder)
                .validOrderCountList(StringUtils.join(validOrderCountList, ","))
                .validOrderCount(totalValidOrder)
                .orderCompletionRate(orderCompletionRate)
                .build();
        return orderReportVO;
    }

    /**
     * 查询销量排名top10
     * @param begin
     * @param end
     * @return
     */
    @Override
    public SalesTop10ReportVO getTop10(LocalDate begin, LocalDate end) {
        Map map=new HashMap();
        map.put("begin",begin);
        map.put("end",end);
        map.put("status",Orders.COMPLETED);

        List<GoodsSalesDTO> top10 = orderMapper.getTop10(map);
        List<String> nameList = top10.stream().map(GoodsSalesDTO::getName).collect(Collectors.toList());
        List<Integer> numberList = top10.stream().map(GoodsSalesDTO::getNumber).collect(Collectors.toList());

        SalesTop10ReportVO salesTop10ReportVO = SalesTop10ReportVO.builder()
                .nameList(StringUtils.join(nameList, ","))
                .numberList(StringUtils.join(numberList, ","))
                .build();
        return salesTop10ReportVO;
    }

    /**
     * 导出Excel运营数据报表
     * @param httpServletResponse
     */
    @Override
    public void exportBusinessData(HttpServletResponse httpServletResponse) {
        LocalDate begin=LocalDate.now().minusDays(30);
        LocalDate end=LocalDate.now().minusDays(1);
        //查询数据库，获取营业表（最近30天）
        BusinessDataVO businessData = workspaceService.getBusinessData(begin,end);
        //通过POI将数据写入excel
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("template/moban.xlsx");

        try {
            //基于模版文件创建一个新的excel文件
            XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
            //获得excel中的sheet
            XSSFSheet sheet = workbook.getSheet("Sheet1");
            //填充日期范围
            sheet.getRow(1).getCell(1).setCellValue("时间："+begin+"至"+end);
            //获得第4行
            XSSFRow row4 = sheet.getRow(3);
            // 填充营业额
            row4.getCell(2).setCellValue(businessData.getTurnover() != null ? businessData.getTurnover() : 0);
            // 填充订单完成率
            row4.getCell(4).setCellValue(businessData.getOrderCompletionRate() != null ? businessData.getOrderCompletionRate() : 0);
            // 填充新增用户数
            row4.getCell(6).setCellValue(businessData.getNewUsers() != null ? businessData.getNewUsers() : 0);
            //获取第5行
            XSSFRow row5 = sheet.getRow(4);
            // 填充有效订单数
            row5.getCell(2).setCellValue(businessData.getValidOrderCount() != null ? businessData.getValidOrderCount() : 0);

            // 填充平均客单价
            row5.getCell(4).setCellValue(businessData.getUnitPrice() != null ? businessData.getUnitPrice() : 0);

            //填充详细数据
            for(int i=0;i<30;i++){
                LocalDate date=begin.plusDays(i);
                //查询某一天的数据
                BusinessDataVO dayBusinessData = workspaceService.getBusinessData(date, date);

                //活得某一行
                XSSFRow row = sheet.getRow(7 + i);
                row.getCell(1).setCellValue(date.toString());
                row.getCell(2).setCellValue(dayBusinessData.getTurnover());
                row.getCell(3).setCellValue(dayBusinessData.getValidOrderCount());
                row.getCell(4).setCellValue(dayBusinessData.getOrderCompletionRate());
                row.getCell(5).setCellValue(dayBusinessData.getUnitPrice());
                row.getCell(6).setCellValue(dayBusinessData.getNewUsers());
            }

            // 向客户端输出Excel文件
            OutputStream outputStream = httpServletResponse.getOutputStream();
            workbook.write(outputStream);
            workbook.close();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
