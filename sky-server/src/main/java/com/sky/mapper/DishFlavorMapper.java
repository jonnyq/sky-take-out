package com.sky.mapper;

import com.sky.entity.DishFlavor;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface DishFlavorMapper {
    /**
     * 插入多条口味记录
     * @param dishFlavors
     */
    void insertFlavors(List<DishFlavor> dishFlavors);

    /**
     * 根据菜品id集合批量删除口味记录
     * @param dishIds
     */
    void deleteDishFlavorByDishIds(List<Long> dishIds);

    /**
     * 根据菜品id删除口味记录
     * @param dishId
     */
    @Delete("delete from dish_flavor where dish_id=#{dishId}")
    void deleteDishFlavorByDishId(Long dishId);

    /**
     * 根据菜品id查询口味信息
     * @param dishId
     * @return
     */
    @Select("select *from dish_flavor where dish_id=#{dishId}")
    List<DishFlavor> getFlavorByDishId(Long dishId);
}
