package com.sky.mapper;

import com.github.pagehelper.Page;
import com.sky.dto.GoodsSalesDTO;
import com.sky.dto.OrdersPageQueryDTO;
import com.sky.entity.Orders;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@Mapper
public interface OrderMapper {
    /**
     * 插入订单数据
     * @param orders
     */
    void insert(Orders orders);

    /**
     * 根据订单号查询订单
     * @param orderNumber
     */
    @Select("select * from orders where number = #{orderNumber}")
    Orders getByNumber(String orderNumber);

    /**
     * 修改订单信息
     * @param orders
     */
    void update(Orders orders);

    /**
     * 各个状态的订单数量统计
     * @return
     */
    @Select(("select count(id) from orders where status=#{status}"))
    Integer countStatus(Integer status);
    /**
     * 查询订单
     * @param id
     * @return
     */
    @Select("select * from orders where id =#{id}")
    Orders getById(Long id);

    /**
     * 订单搜索
     * @param pageQueryDTO
     * @return
     */
    Page<Orders> getByPageQuery(OrdersPageQueryDTO pageQueryDTO);

    /**
     * 查询超时订单
     * @param status
     * @param orderTime
     * @return
     */
    @Select("select * from orders where status=#{status} and order_time < #{orderTime}")
    List<Orders> getByStatusAndOrderTimeLT(Integer status, LocalDateTime orderTime);

    /**
     * 批量动态更新
     * @param ordersList
     */
    void updateBatch(List<Orders> ordersList);

    /**
     * 营业额统计
     * @param map
     * @return
     */
    List<BigDecimal> getTurnover(Map map);

    /**
     * 获取每日订单数列表
     * @param map
     * @return
     */
    List<Integer> countDayOrders(Map map);

    /**
     * 查询销量排名top10
     * @param map
     * @return
     */
    List<GoodsSalesDTO> getTop10(Map map);
}
