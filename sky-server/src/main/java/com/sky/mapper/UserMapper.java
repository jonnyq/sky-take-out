package com.sky.mapper;

import com.sky.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

@Mapper
public interface UserMapper {
    /**
     * 根据openid查询用户
     * @param openid
     * @return
     */
    @Select("select * from user where openid=#{openid}")
    User getByOpenId(String openid);

    /**
     * 根据id查询用户
     * @param id
     * @return
     */
    @Select("select * from user where id=#{id}")
    User getById(Long id);

    /**
     * 注册用户，插入数据
     * @param user
     */
    void insertUser(User user);

    /**
     * 指定时间范围内，查询新用户数量
     * @param begin
     * @param end
     * @return
     */
    List<Integer> countNewUser(LocalDate begin, LocalDate end);

    /**
     * 指定时间范围内，查询总用户数量
     * @param begin
     * @return
     */
    @Select("select count(id) from user where create_time<=DATE(#{begin})")
    Integer countTotalUser(LocalDate begin);
}
