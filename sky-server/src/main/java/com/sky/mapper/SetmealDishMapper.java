package com.sky.mapper;

import com.sky.entity.SetmealDish;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface SetmealDishMapper {
    /**
     * 根据菜品id获取关联套餐ids
     * @param dishIds
     * @return
     */
    List<Long> getSetmealIdsByDishIds(List<Long> dishIds);

    /**
     * 插入相关菜品至关联表
     * @param setmealDishes
     */
    void insertSetmealDishes(List<SetmealDish> setmealDishes);

    /**
     * 根据id查询相关菜品信息
     * @param setmealId
     * @return
     */
    @Select("select * from setmeal_dish where setmeal_id=#{setmealId}")
    List<SetmealDish> getSetmealDishById(Long setmealId);

    /**
     * 根据id在关联表中删除相关菜品信息
     * @param setmealId
     */
    @Delete("delete from setmeal_dish where setmeal_id=#{setmealId}")
    void deleteSetmealDishById(Long setmealId);

    /**
     * 根据不同套餐id在关联表中批量删除相关菜品信息
     * @param ids
     */
    void deleteSetmealDishByIds(List<Long> ids);
}
