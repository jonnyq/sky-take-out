package com.sky.controller.admin;

import com.sky.dto.DishDTO;
import com.sky.dto.DishPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.DishService;
import com.sky.vo.DishVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;

import java.util.List;
import java.util.Set;

/**
 * 菜品管理
 */
@RestController
@RequestMapping("/admin/dish")
@Api(tags = "菜品相关接口")
@Slf4j
public class DishController {
    @Autowired
    private DishService dishService;
    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 新增菜品
     * @param dishDTO
     * @return
     */
    @ApiOperation("新增菜品")
    @PostMapping
    public Result saveDish(@RequestBody DishDTO dishDTO){
        log.info("新增菜品，{}",dishDTO);
        dishService.saveDishWithFlavor(dishDTO);
        //清理缓存数据
        String key="dish_"+dishDTO.getCategoryId();
        cleanCache(key);

        return Result.success();
    }

    /**
     * 菜品分页查询
     * @param dishPageQueryDTO
     * @return
     */
    @ApiOperation("菜品分页查询")
    @GetMapping("/page")
    public Result<PageResult> queryDishByPage(DishPageQueryDTO dishPageQueryDTO){
        log.info("菜品分页查询，{}",dishPageQueryDTO);
        PageResult pageResult = dishService.queryDishByPage(dishPageQueryDTO);

        return Result.success(pageResult);
    }

    /**
     * 批量删除菜品
     * @param ids
     * @return
     */
    @ApiOperation("批量删除菜品")
    @DeleteMapping
    public Result deleteDish(@RequestParam List<Long> ids){
        log.info("批量删除菜品，{}",ids);
        dishService.deleteBatch(ids);
        //redis缓存全清
        cleanCache("dish_*");

        return Result.success();
    }

    /**
     * 根据id获取菜品信息
     * @param id
     * @return
     */
    @ApiOperation("根据id获取菜品信息")
    @GetMapping("/{id}")
    public Result<DishVO> getDishById(@PathVariable Long id){
        log.info("根据id获取菜品信息，{}",id);
        DishVO dishVO= dishService.getDishByIdWithFlavor(id);

        return Result.success(dishVO);
    }

    /**
     * 修改菜品信息
     * @param dishDTO
     * @return
     */
    @ApiOperation("修改菜品信息")
    @PutMapping
    public Result updateDish(@RequestBody DishDTO dishDTO){
        log.info("修改菜品信息，{}",dishDTO);
        dishService.updateDish(dishDTO);

        //redis缓存全清
        cleanCache("dish_*");

        return Result.success();
    }

    /**
     * 根据类别查询菜品
     * @param categoryId
     * @return
     */
    @ApiOperation("根据类别查询菜品")
    @GetMapping("/list")
    public Result getDishByCategoryId(Long categoryId){
        log.info("根据类别查询菜品，{}",categoryId);
        List<Dish>dishList= dishService.getDishByCategoryId(categoryId);

        return Result.success(dishList);
    }

    @ApiOperation("启用禁用菜品")
    @PostMapping("/status/{status}")
    public Result enableOrDisable(@PathVariable Integer status,Long id){
        log.info("启用禁用菜品，{}",id);
        dishService.enableOrDisable(status,id);
        //清理redis
        cleanCache("dish_*");
        return Result.success();
    }

    /**
     * 清理redis缓存
     * @param pattern
     */
    private void cleanCache(String pattern){
        Set keys = redisTemplate.keys(pattern);

        redisTemplate.delete(keys);
    }
}
